<!DOCTYPE html>
<html <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php wp_title(''); ?></title>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
		<?php wp_head(); ?>
		
		
	</head>
	<body <?php body_class();?>>
		<?php wp_body_open(); ?>
		<!-- Navigation  bar start-->
		<nav class="navbar navbar-expand-lg navbar-light bg-light shadow ">
			<div class="container">
				<?php
				if ( function_exists( 'the_custom_logo' ) ) {
				the_custom_logo();
				}
				?>
				
				<!-- 			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<?php $custom_logo_id = get_theme_mod( 'custom_logo' );
				$custom_logo_url = wp_get_attachment_image_url( $custom_logo_id , 'full' );
				echo '<img src="' . esc_url( $custom_logo_url ) . '" alt="xx">'; ?>
				<?php the_custom_logo(); ?>
				<span class="navbar-toggler-icon "></span>
				</button> -->
				<div  id="navbarResponsive">
					<?php
						wp_nav_menu( array(
						'theme_location' => 'my-custom-menu',
						'container_class' => 'custom-menu-class ',
						'menu_class'      => 'navbar-nav ml-auto' ) );
					?>
					
				</div>
				
				
			</div>
			<!-- search form start -->
			<div>
				<?php get_search_form(); ?>
			</div>
			<!-- search form stop -->
		</nav>
		<!-- Navigation  bar End-->
	</body>