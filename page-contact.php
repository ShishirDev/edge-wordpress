<?php get_header(); ?>

	<section class="jumbotron text-center">
    <div class="container">
        <h1 class="jumbotron-heading">CONTACT US</h1>
       
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-8">
            <div class="card">
                <div class="card-header bg-primary text-white"><i class="fa fa-envelope"></i> Contact us.
                </div>
                <div class="card-body">
                   
                        
  							<?php echo do_shortcode('[contact-form-7 id="102" title="Contact form 1"]') ?>
                          
                   
                </div>
            </div>
        </div>
    </div>
       
       <div class="col-4">
            <div class="card bg-light mb-3">
                <div class="card-header badge-primary text-white text-uppercase"><i class="fa fa-home"></i> Address</div>
                <div class="card-body">

                    <p><?php the_title('wordpress edge theme');?></p>
                    <p>Edge india</p>
                    <p>India</p>
                    <p>Email : email@example.com</p>
                    <p>Tel. +91 xxxxxxxxxx</p>

                </div>

            </div>
        </div>
     </div>
  </div>
<?php get_footer(); ?>