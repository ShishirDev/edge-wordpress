<?php 


if(!isset($content_width)){
	$content_width = 690;
}

 /*
function loadDirectory() { ?>
<script type="text/javascript">
    var theme_directory = "<?php echo get_template_directory_uri() ?>";
</script> 
<?php } 
add_action('wp_head', 'loadDirectory'); 
*/
function edge_wp_setup(){
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'custom-header' );
	add_theme_support( 'custom-logo' );
	// require_once get_template_directory() . '/class/class-wp-bootstrap-navwalker.php';

	// register_nav_menus( array(
	//     'primary' => __( 'Primary Menu', 'edge-wordpress' ),

	//     'footer' =>  __( 'Footer Menu', 'edge-wordpress' )

	// ) );



	//Important (Don't delete)
// 	register_sidebar( array(
//     'name' => __( 'Header information', 'twentytwelve' ),
//     'id' => 'header-information',
//     'description' => __( 'Information for the header', 'twentytwelve' ),
//     'before_widget' => '<aside id="%1$s" class="widget %2$s">',
//     'after_widget' => '</aside>',
//     'before_title' => '<h3 class="widget-title">',
//     'after_title' => '</h3>',
// ) );


//Use the code below in pages to implement register_sidebar 
	// <!-- <?php dynamic_sidebar( 'header-information' ); 
	//

}

add_action('after_setup_theme','edge_wp_setup');

	
function wpb_custom_new_menu() {
  register_nav_menu('my-custom-menu',__( 'My Custom Menu' ));
}
add_action( 'init', 'wpb_custom_new_menu' );

function edge_scripts(){
	// Style
	wp_enqueue_style('bootstrap-core',get_template_directory_uri().'/css/bootstrap.min.css');
	wp_enqueue_style('custom',get_template_directory_uri().'/style.css');

// Scripts
	wp_enqueue_script('bootstrap-js',get_template_directory_uri().'/js/bootstrap.min.js',array('jquery'),true);

	wp_enqueue_script('main-js',get_template_directory_uri().'/js/main.js',array('jquery'),true);
}

add_action('wp_enqueue_scripts','edge_scripts');






// Replaces the excerpt "Read More" text by a link
function new_excerpt_more($more) {
       global $post;
//	return '<a class="moretag" href="'. get_permalink($post->ID) . '"> Read the full article...</a>';
       return '..';
}
add_filter('excerpt_more', 'new_excerpt_more');




/**
 * Register our sidebars and widgetized areas.
 *
 */
function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'Home right sidebar',
		'id'            => 'home_right_1',
		'before_widget' => '<center>',
		'after_widget'  => '</center>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );

//Custom Header Support
$args = array(
	// 'width'         => 980,
	// 'height'        => 60,
	'default-image' => get_template_directory_uri() . '/images/edge-wordpress2.jpg',
	'uploads'                => true,
);
add_theme_support( 'custom-header', $args );




 // Add custom Logo
function themename_custom_logo_setup() {
 $defaults = array(
 'height'      => 50,
 'width'       => 120,
 'flex-height' => true,
 'flex-width'  => true,
 'header-text' => array( 'site-title', 'site-description' ),
 );
 add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'themename_custom_logo_setup' );


function theme_prefix_setup() {
	
	add_theme_support( 'custom-logo', array(
		'height'      => 50,
		'width'       => 120,
		'flex-width' => true,
	) );

}
add_action( 'after_setup_theme', 'theme_prefix_setup' );

function theme_prefix_the_custom_logo() {
	
	if ( function_exists( 'the_custom_logo' ) ) {
		the_custom_logo();
	}

}

?>


