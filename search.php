<?php get_header(); ?>
<div id="primary" class="content-area container mt-5">
	<main id="main" class="site-main">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<div class="container">
			<div class="row">
				<!-- Blog Entries Column -->
				<div class="col-md-8">
					
					<!-- Blog Post -->
					<div class="card mb-4">
						
						<div class="card-body">
							<h2 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
							<p><i class="fa fa-tag"></i><?php the_tags(); ?></p>
							<p><i class="fa fa-folder-open"></i> <?php the_category(','); ?></p>
					
							<?php the_post_thumbnail('large',array('class' => 'img-fluid')); ?>
							
							<p class="card-text"><?php the_content(); ?></p>
							
						</div>
						<div class="card-footer text-muted">
							Posted on <?php echo get_the_date('F j,Y'); ?> by
							<a href="#"><?php the_author(); ?></a>
						</div>
					</div>
				</div>
				<?php comments_template(); ?>
				
			</div>
			<!-- /.row -->
		</div>
		
		
		
		<?php endwhile; else : ?>
		<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
		<?php endif; ?>
	</main>
</div>
<?php get_footer(); ?>