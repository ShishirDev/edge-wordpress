<div class="col-md-4" style="margin-top: -15px">

        <!-- Dynamic Widget -->
         <div class="card my-3">
          <h5 class="card-header">Calender</h5>
          <div class="card-body">
            <div class="row">
              <div class="col-lg-8">
                
                   <?php if ( is_active_sidebar( 'home_right_1' ) ) : ?>
          <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
            <?php dynamic_sidebar( 'home_right_1' ); ?>
          </div><!-- #primary-sidebar -->
        <?php endif; ?>
               
              </div>
    
            </div>
          </div>
        </div>
         
        
    <!-- Archives Widget -->
        <div class="card my-4">
          <h5 class="card-header">Archives</h5>
          <div class="card-body">
            <div class="row">
              <div class="col-lg-8">
                <ul class="list-unstyled mb-0">
                  <?php wp_get_archives(); ?>
                </ul>
              </div>
    
            </div>
          </div>
        </div>
        <!-- Categories Widget -->
        <div class="card my-4">
          <h5 class="card-header">Categories</h5>
          <div class="card-body">
            <div class="row">
              <div class="col-lg-8">
                <ul class="list-unstyled mb-0">
                  <?php wp_list_categories('title_li='); ?> 
                </ul>
              </div>
    
            </div>
          </div>
        </div>



      </div>