<?php get_header(); ?>
<header>
	<div id="carouselExampleIndicators" class="carousel slide" >
		
		<div class="carousel-inner" role="listbox">
			<!-- Slide One - Set the background image for this slide in the line below -->
			<div class="carousel-item active">
				<img src="<?php header_image(); ?>"  alt="" />
				<div class="carousel-caption d-block" style="margin-bottom: 100px">
					<h2 class="display-4 "><?php the_field('feature_text'); ?></h2>
					<p class="lead"><?php the_field('description_text'); ?></p>
				</div>
			</div>
			<!-- Slide One end -->
		</div>
	</div>
</header>
<!-- Services-->
<section>
	<div class="container">
		<br><br>
		<center><h2><?php the_field('services_text')?></h2></center>
		<br>
		<div class="row">
			<div class="col-sm-6 col-lg-4 mb-3">
				<svg class="lnr text-primary services-icon">
					<use xlink:href="#lnr-magic-wand"></use>
				</svg>
				<h6>Ex cupidatat eu</h6>
				<p class="text-muted">Ex cupidatat eu officia consequat incididunt labore occaecat ut veniam labore et cillum id et.</p>
			</div>
			<div class="col-sm-6 col-lg-4 mb-3">
				<svg class="lnr text-primary services-icon">
					<use xlink:href="#lnr-heart"></use>
				</svg>
				<h6>Tempor aute occaecat</h6>
				<p class="text-muted">Tempor aute occaecat pariatur esse aute amet.</p>
			</div>
			<div class="col-sm-6 col-lg-4 mb-3">
				<svg class="lnr text-primary services-icon">
					<use xlink:href="#lnr-rocket"></use>
				</svg>
				<h6>Voluptate ex irure</h6>
				<p class="text-muted">Voluptate ex irure ipsum ipsum ullamco ipsum reprehenderit non ut mollit commodo.</p>
			</div>
			<div class="col-sm-6 col-lg-4 mb-3">
				<svg class="lnr text-primary services-icon">
					<use xlink:href="#lnr-paperclip"></use>
				</svg>
				<h6>Tempor commodo</h6>
				<p class="text-muted">Tempor commodo nostrud ex Lorem occaecat duis occaecat minim.</p>
			</div>
			<div class="col-sm-6 col-lg-4 mb-3">
				<svg class="lnr text-primary services-icon">
					<use xlink:href="#lnr-screen"></use>
				</svg>
				<h6>Et fugiat sint occaecat</h6>
				<p class="text-muted">Et fugiat sint occaecat voluptate incididunt anim nostrud ea cillum cillum consequat.</p>
			</div>
			<div class="col-sm-6 col-lg-4 mb-3">
				<svg class="lnr text-primary services-icon">
					<use xlink:href="#lnr-inbox"></use>
				</svg>
				<h6>Et labore tempor et</h6>
				<p class="text-muted">Et labore tempor et adipisicing dolor.</p>
			</div>
		</div>
	</div>
</section>
<!-- Services end-->
<br><br>
<!-- Portfolio start-->
<section class="bg-light">
	<div class="container">
		<br><br><h2><?php the_field('portfolio_text')?></h2>
		<p class="lead text-muted mb-5">In enim non sit irure ut adipisicing laboris et laborum.</p>
		<div class="row">
			<div class="col-md-4 mb-4">
				<div class="card shadow border-0 h-100"><a href="#"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/mockup1.jpg" alt="" class="card-img-top"></a>
				<div class="card-body">
					<h5> <a href="#" class="text-dark">Ex cupidatat eu</a></h5>
					<p class="text-muted card-text">Ex cupidatat eu officia consequat incididunt labore occaecat ut veniam labore et cillum id et.</p>
					<p class="card-text"><a href="#">Read more</a></p>
				</div>
			</div>
		</div>
		<div class="col-md-4 mb-4">
			<div class="card shadow border-0 h-100"><a href="#"><img src="<?php echo esc_url(get_template_directory_uri());?>/images/mockup2.jpg" alt="" class="card-img-top"></a>
			<div class="card-body">
				<h5> <a href="#" class="text-dark">Tempor aute occaecat</a></h5>
				<p class="text-muted card-text">Tempor aute occaecat pariatur esse aute amet.</p>
				<p class="card-text"><a href="#">Read more</a></p>
			</div>
		</div>
	</div>
	<div class="col-md-4 mb-4">
		<div class="card shadow border-0 h-100"><a href="#"><img src="<?php echo esc_url(get_template_directory_uri());?>/images/mockup3.jpg" alt="" class="card-img-top"></a>
		<div class="card-body">
			<h5> <a href="#" class="text-dark">Voluptate ex irure</a></h5>
			<p class="text-muted card-text">Voluptate ex irure ipsum ipsum ullamco ipsum reprehenderit non ut mollit commodo.</p>
			<p class="card-text"><a href="#">Read more</a></p>
		</div>
	</div>
</div>
<div class="col-md-4 mb-4">
	<div class="card shadow border-0 h-100"><a href="#"><img src="<?php echo esc_url(get_template_directory_uri());?>/images/mockup4.jpg" alt="" class="card-img-top"></a>
	<div class="card-body">
		<h5> <a href="#" class="text-dark">Tempor commodo</a></h5>
		<p class="text-muted card-text">Tempor commodo nostrud ex Lorem occaecat duis occaecat minim.</p>
		<p class="card-text"><a href="#">Read more</a></p>
	</div>
</div>
</div>
<div class="col-md-4 mb-4">
<div class="card shadow border-0 h-100"><a href="#"><img src="<?php echo esc_url(get_template_directory_uri());?>/images/mockup5.jpg" alt="" class="card-img-top"></a>
<div class="card-body">
	<h5> <a href="#" class="text-dark">Et fugiat sint occaecat</a></h5>
	<p class="text-muted card-text">Et fugiat sint occaecat voluptate incididunt anim nostrud ea cillum cillum consequat.</p>
	<p class="card-text"><a href="#">Read more</a></p>
</div>
</div>
</div>
<div class="col-md-4 mb-4">
<div class="card shadow border-0 h-100"><a href="#"><img src="<?php echo esc_url(get_template_directory_uri());?>/images/mockup6.jpg" alt="" class="card-img-top"></a>
<div class="card-body">
<h5> <a href="#" class="text-dark">Et labore tempor et</a></h5>
<p class="text-muted card-text">Et labore tempor et adipisicing dolor.</p>
<p class="card-text"><a href="#">Read more</a></p>
</div>
</div>
</div>
</div>
</div>
</section>
<!-- Portfolio end-->
<!-- Quote-->
<br>
<section>
<div class="container">
<blockquote class="blockquote text-center mb-0">
<svg class="lnr text-muted quote-icon pull-left">
<use xlink:href="#lnr-heart"> </use>
</svg>
<p class="mb-0">There is no place like 127.0.0.1</p>
<footer class="blockquote-footer">Someone famous in
<cite title="Source Title">Source Title</cite>
</footer>
</blockquote>
</div>
</section>
<!-- Quote end -->
<br><br>
<?php get_footer(); ?>