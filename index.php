<?php get_header(); ?>

	<div id="primary" class="content-area container mt-5">
		<main id="main" class="site-main">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<div class="container">

				    <div class="row">
				      <!-- Blog Entries Column -->
				      <div class="col-md-8">

				        <!--<h3 class="my-4"><?php the_title(); ?>
				           
				        </h3>-->

				        <!-- Blog Post -->
				        <div class="card mb-4">

				          <!-- <img class="card-img-top" src="http://placehold.it/750x300" alt="Card image cap"> -->
				          <div class="card-body blog-post">
				            <h2 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				            <p><i class="fa fa-tag"></i><?php the_tags(); ?></p>
				            <p><i class="fa fa-folder-open"></i> <?php the_category(','); ?></p>
				            <div class="blog-img"><?php the_post_thumbnail('medium_large'); ?></div>
				            <p class="card-text"><?php the_excerpt(); ?></p>
				            <a href="<?php echo get_permalink(); ?>" class="btn btn-primary">Read More →</a>
				          </div>
				          <div class="card-footer text-muted">
				            Posted on <?php echo get_the_date('F j,Y'); ?> by
				            <a href="#"><?php the_author(); ?></a>
				          </div>
				        </div>



				      </div>

				      <!-- Sidebar Widgets Column -->
				  			<?php get_sidebar(); ?>
				    </div>
				    <!-- /.row -->

				  </div>
						

		

		
		<?php endwhile; else : ?>
			<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
		<?php endif; ?>

				         <!-- Pagination  -->
				        <ul class="pagination justify-content-center mb-4">

				          <li class="page-item mr-3">
				          	<?php previous_posts_link(' <button class="btn btn-primary">Newer Posts</button>'); ?>
				          </li>
				          <li class="page-item ">
				          	<?php next_posts_link('<button class="btn btn-primary">Older Posts</button>'); ?>
				          </li>
				        </ul>


				       
	</main>
</div>



<?php get_footer(); ?>