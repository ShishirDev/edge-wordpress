<!-- Footer-->
<div class="py-5 bg-light">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 mb-4 mb-lg-0">
        <h5 >Edge-word</h5>
        <ul class="contact-info list-unstyled">
          <li><a href="<?php the_field('footer_text', 9)?>" class="text-dark">
<!-- postid is must. pass the postid as second pararmeter. Get postID from pages url             the_field( "footer_text", $postID); -->
          
<?php 
            the_field( "footer_text", 9);

            ?>
          </a></li>
          <li><a href="tel:123456789" class="text-dark">+00 123 456 789</a></li>
        </ul>
        <p class="text-muted">Laborum aute enim consectetur eu laboris commodo.</p>
      </div>
      <div class="col-lg-4 col-md-6 ">
        <h5>Pages</h5>
        <div class="footer-menu">

          <?php
            wp_nav_menu( array( 
                'theme_location' => 'my-custom-menu', 
                'container_class' => 'custom-menu-footer-class',
                'menu_class'      => 'navbar-nav ml-auto' ) ); 
            ?>
  
        </div>
      </div>
    </div>
  </div>
</div>

<div class="py-3 bg-light ">
  <hr>
  <div class="container">
    <div class="row">
      <div class="col-md-7 text-center text-md-left">
        <p class="mb-md-0">&copy; <?php echo date('Y'); ?> Your company. All rights reserved. </p>
      </div>
      <div class="col-md-5 text-center text-md-right">
        <p class="mb-0">Template By <a href="https://bitflextechnologies.com/"><?php echo get_bloginfo('name'); ?></a></p>
      </div>
    </div>
  </div>
</div>
<!-- J-query -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.linearicons.com/free/1.0.0/svgembedder.min.js"></script>
<?php wp_footer(); ?>
</body>
</html>