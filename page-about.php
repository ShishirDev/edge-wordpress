
<?php get_header(); ?>

<!-- <section class="jumbotron text-gray-dark text-left">
    <div class="container">
        <h1 class="jumbotron-heading">About us</h1>
       
    </div>
</section> -->
<div class="image-aboutus-banner"style="margin-top:70px">
   <div class="container">
    <div class="row">
        <div class="col-md-12">
         <h1 class="lg-text">About Us</h1>
         <p class="image-aboutus-para">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
       </div>
    </div>
</div>
</div>
<div class="bread-bar">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-8 col-sm-6 col-xs-8">
                    <ol class="breadcrumb">
                        <li><a href="index.html">Home</a></li>
                        <li class="active">About Us</li>
                    </ol>
            	</div>
                <div class="col-md-4 col-sm-6 col-xs-4">
                </div>
            </div>
      	</div>
    </div>
<div class="aboutus-secktion paddingTB60">
    <div class="container">
        <div class="row">
                	<div class="col-md-6">
                    	<h1 class="strong">Who we are and<br>what we do</h1>
                        <p class="lead">This is the world's leading portal for<br>easy and quick </p>
                    </div>
                    <div class="col-md-6">
                    	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel. Donec vel mauris quam.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel. Donec vel mauris quam. Lorem ipsum dolor sit amet. Nulla convallis egestas rhoncus.</p>
                    </div>
</div>
    </div>
</div>

<!-- Header -->
<header class="badge-light text-center py-5 mb-4">
	<div class="container">
		<h1 class="font-weight-bold text-gray-dark">Meet the Team</h1>
	</div>
</header>

<!-- Page Content -->
<div class="container">
	<div class="row">
		<!-- Team Member 1 -->
		<div class="col-xl-3 col-md-6 mb-4">
			<div class="card border-0 shadow">
				<img src="https://source.unsplash.com/TMgQMXoglsM/500x350" class="card-img-top" alt="...">
				<div class="card-body text-center">
					<h5 class="card-title mb-0">Team Member</h5>
					<div class="card-text text-black-50">Web Developer</div>
				</div>
			</div>
		</div>
		<!-- Team Member 2 -->
		<div class="col-xl-3 col-md-6 mb-4">
			<div class="card border-0 shadow">
				<img src="https://source.unsplash.com/9UVmlIb0wJU/500x350" class="card-img-top" alt="...">
				<div class="card-body text-center">
					<h5 class="card-title mb-0">Team Member</h5>
					<div class="card-text text-black-50">Web Developer</div>
				</div>
			</div>
		</div>
		<!-- Team Member 3 -->
		<div class="col-xl-3 col-md-6 mb-4">
			<div class="card border-0 shadow">
				<img src="https://source.unsplash.com/sNut2MqSmds/500x350" class="card-img-top" alt="...">
				<div class="card-body text-center">
					<h5 class="card-title mb-0">Team Member</h5>
					<div class="card-text text-black-50">Web Developer</div>
				</div>
			</div>
		</div>
		<!-- Team Member 4 -->
		<div class="col-xl-3 col-md-6 mb-4">
			<div class="card border-0 shadow">
				<img src="https://source.unsplash.com/ZI6p3i9SbVU/500x350" class="card-img-top" alt="...">
				<div class="card-body text-center">
					<h5 class="card-title mb-0">Team Member</h5>
					<div class="card-text text-black-50">Web Developer</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>